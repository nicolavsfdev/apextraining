// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ConsumptionRate {
    global Id Id;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    /* Este índice es una parte de esta programación.
    */
    global ConsumptionSchedule ConsumptionSchedule;
    /* Este índice es una parte de esta programación.
    */
    global Id ConsumptionScheduleId;
    global String Description;
    /* El orden para el procesamiento del índice de uso entre múltiples índices. Los índices de consumo se evalúan comenzando con el orden de procesamiento más bajo.
    */
    global Integer ProcessingOrder;
    /* Seleccione si se asigna un precio al uso por unidad o cantidad o como una tarifa plana.
    */
    global String PricingMethod;
    /* La cantidad más baja de uso para este índice de consumo.
    */
    global Integer LowerBound;
    /* La cantidad más alta de uso para este índice de consumo.
    */
    global Integer UpperBound;
    /* El precio para el uso que entra dentro de los límites de este índice de consumo.
    */
    global Decimal Price;
    global List<ConsumptionRateHistory> Histories;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<FlowRecordRelation> RelatedRecord;

    global ConsumptionRate () 
    {
    }
}