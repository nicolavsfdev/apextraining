// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ConsumptionSchedule {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Boolean IsActive;
    global String Description;
    /* Número de unidades de facturación a las que se aplica esta programación de consumo. Por ejemplo, si la unidad de plazo de facturación es Mes, introduzca 12 para aplicar las programaciones de consumo para 12 meses. Aplique programaciones de consumo a un máximo de 20 trimestres, 60 meses o 5 años.
    */
    global Integer BillingTerm;
    /* La unidad utilizada con el plazo de facturación para determinar la frecuencia de facturación.
    */
    global String BillingTermUnit;
    /* Índice: Los precios de programación utilizando únicamente el nivel que se aplica a la cantidad de uso. Bloque: El uso en un límite dado recibe precios iguales al valor de su nivel.
    */
    global String Type;
    /* Las cantidades de uso y los índices se aplican a esta unidad de medición.
    */
    global String UnitOfMeasure;
    /* Seleccione un caso de uso específico para calificar el uso en esta programación. Ésta es una lista de selección de control para el campo Tipo.
    */
    global String RatingMethod;
    /* El uso se compara con una programación de consumo si los registros comparten el mismo valor Atributo de coincidencia.
    */
    global String MatchingAttribute;
    global Integer NumberOfRates;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ConsumptionRate> ConsumptionRates;
    global List<ConsumptionScheduleFeed> Feeds;
    global List<ConsumptionScheduleHistory> Histories;
    global List<ConsumptionScheduleShare> Shares;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<ProductConsumptionSchedule> ProductConsumptionSchedules;
    global List<ContentVersion> FirstPublishLocation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;

    global ConsumptionSchedule () 
    {
    }
}