// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ExternalDataSource {
    global Id Id;
    global Boolean IsDeleted;
    global String DeveloperName;
    global String Language;
    global String MasterLabel;
    global String NamespacePrefix;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String Type;
    /* La URL para conectar con el sistema externo.
    */
    global String Endpoint;
    global String Repository;
    global Boolean IsWritable;
    /* Utilizado para seguir a los usuarios que acceden al servidor externo. Anónimo implica que no se ha especificado la identidad de un usuario para el acceso al servidor externo. Principal nombrado utiliza una identidad de usuario para todos los usuarios para acceder al servidor externo.
    */
    global String PrincipalType;
    /* Especifique cómo Salesforce debe autenticar para el servidor externo. La autenticación de contraseña indica que se requiere un nombre de usuario o una contraseña para autenticar. Sin autenticación indica que no se requieren credenciales.
    */
    global String Protocol;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global AuthProvider AuthProvider;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global Id AuthProviderId;
    global StaticResource LargeIcon;
    global Id LargeIconId;
    global StaticResource SmallIcon;
    global Id SmallIconId;
    global String CustomConfiguration;
    global List<CustomHttpHeader> CustomHttpHeaders;
    global List<ExternalDataUserAuth> UserAuths;
    global List<SetupEntityAccess> SetupEntityAccessItems;
    global List<ContentVersion> ExternalDataSource;

    global ExternalDataSource () 
    {
    }
}