// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class NamedCredential {
    global Id Id;
    global Boolean IsDeleted;
    global String DeveloperName;
    global String Language;
    global String MasterLabel;
    global String NamespacePrefix;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    /* La URL para conectar con el sistema externo.
    */
    global String Endpoint;
    /* Utilizado para seguir a los usuarios que acceden al servidor externo. Anónimo implica que no se ha especificado la identidad de un usuario para el acceso al servidor externo. Principal nombrado utiliza una identidad de usuario para todos los usuarios para acceder al servidor externo.
    */
    global String PrincipalType;
    global Boolean CalloutOptionsGenerateAuthorizationHeader;
    global Boolean CalloutOptionsAllowMergeFieldsInHeader;
    global Boolean CalloutOptionsAllowMergeFieldsInBody;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global AuthProvider AuthProvider;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global Id AuthProviderId;
    global String JwtIssuer;
    global String JwtFormulaSubject;
    global String JwtTextSubject;
    global Integer JwtValidityPeriodSeconds;
    global String JwtAudience;
    global String AuthTokenEndpointUrl;
    global List<CustomHttpHeader> CustomHttpHeaders;
    global List<ExternalDataUserAuth> UserAuths;
    global List<SetupEntityAccess> SetupEntityAccessItems;
    global List<UserProvisioningConfig> NamedCredential;

    global NamedCredential () 
    {
    }
}