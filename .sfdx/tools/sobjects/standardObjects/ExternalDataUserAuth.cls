// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ExternalDataUserAuth {
    global Id Id;
    global Boolean IsDeleted;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global SObject ExternalDataSource;
    global Id ExternalDataSourceId;
    global User User;
    global Id UserId;
    /* Especifique cómo Salesforce debe autenticar para el servidor externo. La autenticación de contraseña indica que se requiere un nombre de usuario o una contraseña para autenticar. Sin autenticación indica que no se requieren credenciales.
    */
    global String Protocol;
    global String Username;
    global String Password;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global AuthProvider AuthProvider;
    /* Este servicio de Salesforce o externo proporciona el proceso de inicio de sesión y aprueba el acceso al sistema externo.
    */
    global Id AuthProviderId;

    global ExternalDataUserAuth () 
    {
    }
}